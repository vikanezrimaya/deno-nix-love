{
  description = "Experiments in building Deno packages";

  inputs = {
    nixpkgs = {}; # unpinned by design, pin this yourself for now
  };

  outputs = { self, nixpkgs }: {
    packages = let
      mkDenoPackage' = { stdenvNoCC, lib, runCommandNoCC, deno }:
      { pname, version, src
      , denoPermissions ? [], cacheHash
      , denoUnstable ? false, denoImportMap ? null
      , ... }@args: let
        denoCache = runCommandNoCC "deno-cache-${pname}-${version}" {
          inherit src;
          outputHash = cacheHash;
          outputHashAlgo = null;
          outputHashMode = "recursive";
          buildInputs = [ deno ];
        } ''
          export DENO_DIR=$out
          cd $src
          deno cache \
            ${lib.optionalString (denoImportMap != null) ("--import-map " + (lib.escapeShellArg denoImportMap))} \
            ${lib.optionalString denoUnstable "--unstable"} \
            ./mod.ts
        '';
      in stdenvNoCC.mkDerivation (rec {
        inherit pname version src;

        configurePhase = "";
        buildPhase = "";
        installPhase = ''
          mkdir -p $out/bin $out/lib/deno
          cp -r . $out/lib/deno/${pname}
          cat > $out/bin/${pname} << EOF
          #!${stdenvNoCC.shell}
          DENO_DIR=${denoCache} ${deno}/bin/deno run
            ${lib.optionalString denoUnstable "--unstable"} \
            ${lib.optionalString (denoImportMap != null) ("--import-map " + (lib.escapeShellArg denoImportMap))} \
            ${lib.concatStringsSep " " (map (str: "--allow-" + str) denoPermissions)} \
            ${src}/mod.ts "$@"
          EOF
        '';

        checkPhase = ''
          export DENO_DIR=${denoCache}
          deno test
            ${lib.optionalString (denoImportMap != null) ("--import-map " + (lib.escapeShellArg denoImportMap))} \
            ${lib.optionalString denoUnstable "--unstable"} \
            ./mod.ts
        '';

      } // args // {
        meta = {
          platforms = deno.meta.platforms;
        } // (args.meta or {});
        passthru = { inherit denoCache; } // (args.passthru or {});
      });
      hello-deno = { mkDenoPackage, lib, copyPathToStore }: mkDenoPackage rec {
        pname = "rpglib";
        version = "0.0.0";
        cacheHash = "sha256-jLQEcdxxaJC9ADGE24TVIa2TWR/KiLZi8vzUvW1p5c0=";
        denoPermissions = ["net"];

        # Prevents the flake source from poisoning the input and changing the hash
        # Supposedly...
        src = copyPathToStore ./hello-deno;

        meta = with lib; {
          maintainers = with maintainers; [ vika_nezrimaya ];
        };
      };
    in nixpkgs.lib.genAttrs ["x86_64-linux" "aarch64-linux"] (system: let
      pkgs = nixpkgs.legacyPackages.${system};
      mkDenoPackage = pkgs.callPackage mkDenoPackage' {};
    in {
      hello-deno = pkgs.callPackage hello-deno { inherit mkDenoPackage; };
    });
  };
}
